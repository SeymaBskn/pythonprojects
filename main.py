import os
import sys

from PIL import Image, ImageFilter
from pathlib import Path


current_directory = os.getcwd()

image_folder = sys.argv[1]
output_folder = sys.argv[2]

Path(output_folder).mkdir(parents=True, exist_ok=True)

# images = os.listdir(path_for_source_images)

# os.mkdir(path_for_converted_images)

for image in os.listdir(image_folder):
    main_image = Image.open(f"{image_folder}{image}")
    prefix = os.path.splitext(image)[0]
    main_image.save(f"{output_folder}{prefix}.png", "png")

